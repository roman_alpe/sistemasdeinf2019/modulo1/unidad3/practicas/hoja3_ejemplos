﻿/**DROP DATABASE IF EXISTS ej_programacion3;**/
CREATE DATABASE ej_programacion3;
USE ej_programacion3;

/** Ejemplo 2.
-Realizar una función que reciba como argumentos:-

- Base de un triangulo
- Altura de un triangulo

Debe devolver el cálculo del área del triángulo.
**/

DELIMITER //
  CREATE OR REPLACE FUNCTION areaTriangulo(base float, altura float)
    RETURNS float
    BEGIN
       
      DECLARE var float DEFAULT 0;

      SET var = (base * altura)/2;

      RETURN var;
    END //
  DELIMITER ;

  SELECT areaTriangulo(5,4);

/** 
3.- Ejemplo 3. 
-Función perímetro triangulo-

Realizar una función que reciba como argumentos:

- Base de un triangulo
- Lado2 de un triangulo
- Lado3 de un triangulo

Debe devolver el cálculo del perímetro del triángulo.
**/
 DELIMITER //
  CREATE OR REPLACE FUNCTION perimetroTriangulo(base float, lado2 float, lado3 float)
    RETURNS float
    BEGIN
      
      DECLARE perimetr float DEFAULT 0;

      SET perimetr = base + lado2 + lado3;

      RETURN perimetr;
    END //
  DELIMITER ;

  SELECT perimetroTriangulo(6,6,4);

/**
Ejemplo 4. 
  -Procedimiento almacenado triángulos- 

Realizar un procedimiento almacenado 
que cuando le llames como argumentos: 

- Id1: id inicial 
- Id2: id final 

Actualice el área y el perímetro de los triángulos 
(utilizando las funciones realizadas)
que estén comprendidos entre los id pasados. 
**/
DELIMITER //
  CREATE OR REPLACE PROCEDURE actualTriangulos(id1 int, id2 int)
    BEGIN
      
      UPDATE triangulos SET 
        area = arTriangl(base, altura),
        perimetro = perTriangl(base, lado2, lado3) 
      WHERE id BETWEEN id1 AND id2;
    END //
  DELIMITER ;

  CALL actualTriangulos(4,16);

  SELECT * FROM triangulos t;
/**
Ejemplo 5.
-Función área cuadrado- 

Realizar una función que reciba como argumentos: 

- Lado de un cuadrado 
Debe devolver el cálculo del área 

**/
DELIMITER //
CREATE OR REPLACE FUNCTION arsquare(side float)
  RETURNS float
   
  BEGIN 
    DECLARE var float DEFAULT 0;
  
        SET var = POW(side,2);
  
        RETURN var;

  END //
DELIMITER ;
SELECT arsquare(16);
/**
Ej 6.
-Función perímetro cuadrado

Realizar una función que reciba como argumentos:

- Lado de un cuadrado

Debe devolver el cálculo del perímetro
**/
DELIMITER //
  CREATE OR REPLACE FUNCTION perimtrCuadr(lado float)
    RETURNS float
    BEGIN
      
      DECLARE vperimtr float DEFAULT 0;

      SET vperimtr = lado * 4;

      RETURN vperimtr;
    END //
  DELIMITER ;

  SELECT perimtrCuadr(24);


/**
Ej 7.-Procedimiento almacenado cuadrados
Realizar un procedimiento almacenado 
que cuando le llames como argumentos:
- Id1: id inicial
- Id2: id final
Actualice el área y el perímetro de los cuadrados 
(utilizando las funciones realizadas) 
que estén comprendidos entre los id pasados. 
**/

  DELIMITER //
  CREATE OR REPLACE PROCEDURE actualCuadr(id1 int, id2 int)
    BEGIN
        
      UPDATE cuadrados SET
        ar = arCuadr(lado),
        per = perCuadr(lado)
      WHERE id BETWEEN id1 AND id2;

    END //
  DELIMITER ;

  CALL actualCuadr(6,18);

  SELECT * FROM cuadrados c;

/**
8.- Función área rectángulo

Realizar una función que reciba como argumentos:

- Lado1
- Lado2

Debe devolver el cálculo del área

**/
DELIMITER //
  CREATE OR REPLACE FUNCTION arRect(lado1 float, lado2 float)
    RETURNS float
    BEGIN
      
      DECLARE var float DEFAULT 0;

      SET var = lado1 * lado2;

      RETURN var;
    END //
  DELIMITER ;

  SELECT arRect(8,10);
/**
9.- Función perímetro rectángulo
Realizar una función que reciba como argumentos:
- Lado1
- Lado2
Debe devolver el cálculo del perímetro
**/
DELIMITER //
  CREATE OR REPLACE FUNCTION perRect(lado1 float, lado2 float)
    RETURNS float
    BEGIN
      
      DECLARE vper float DEFAULT 0;

      SET vper = (lado1 + lado2) * 2;

      RETURN vper;
    END //
  DELIMITER ;

  SELECT perRect(8,11);
/**
10.- Procedimiento almacenado rectángulo
Realizar un procedimiento almacenado 
que cuando le llames como argumentos:
- Id1: id inicial
- Id2: id final
Actualice el área y el perímetro de los rectángulos 
(utilizando las funciones realizadas) 
que estén comprendidos entre los id pasados.
**/
DELIMITER //
  CREATE OR REPLACE PROCEDURE actualRect(id1 int, id2 int)
    BEGIN

      UPDATE rectangulo set
        ar = arRect(lado1, lado2),
        per = perRect(lado1, lado2)
      WHERE id BETWEEN id1 AND id2; 
      
    END //
  DELIMITER ;

  CALL actualRect(1,8);

  SELECT * FROM rectangulo r;

/**
11.- Función área circulo
Realizar una función que reciba como argumentos:
- radio
Debe devolver el cálculo del área
**/
 DELIMITER //
  CREATE OR REPLACE FUNCTION arCirc(radio float)
    RETURNS float
    BEGIN
      
      DECLARE var float DEFAULT 0;

      SET var =  PI()*POW(radio,2);                        

      RETURN var;
    END //
  DELIMITER ;

  SELECT arCirc(8);


/**
12.- Función perímetro circulo
Realizar una función que reciba como argumentos:
- radio
Debe devolver el cálculo del perímetro
**/

DELIMITER //
  CREATE OR REPLACE FUNCTION perCirc(radio float)
    RETURNS float
    BEGIN
      
      DECLARE vper float DEFAULT 0;

      SET vper = 2 * PI() * radio;

      RETURN vper;
    END //
  DELIMITER ;

  SELECT perCirc(4);

/**
13.- Procedimiento almacenado círculos
Realizar un procedimiento almacenado 
que cuando le llames como argumentos:

- Id1: id inicial
- Id2: id final
- Tipo: puede ser a, b o null.

Actualice el área y el perímetro de los círculos 
(utilizando las funciones realizadas) 
que estén comprendidos entre los id
pasados y que sea del tipo pasado. 
Si el tipo es null cogerá todos los tipos.
**/
DELIMITER //
  CREATE OR REPLACE PROCEDURE actualCirc(id1 int, id2 int, tipo char(1))
    BEGIN
      
      IF (tipo IS NULL) THEN
        UPDATE circulo SET
          ar = arCirc(radio),
          per = perCirc(radio)
        WHERE id BETWEEN id1 AND id2;
      ELSE
        UPDATE circulo c SET
          ar = arCirc(radio),
          per = perCirc(radio)
        WHERE id BETWEEN id1 AND id2 
        AND c.tipo = tipo;
      END IF;

    END //
  DELIMITER ; 

  CALL actualCirc(7,10,'a');
  SELECT * FROM circulo c;
  CALL actualCirc(9,20,'b');
  SELECT * FROM circulo c;
  CALL actualCirc(17,20,null);
  SELECT * FROM circulo c;
/**
14.- Función media
Realizar una función que reciba como argumentos 4 notas.
Debe devolver el cálculo de la nota media.
**/
DELIMITER //
  CREATE OR REPLACE FUNCTION media(nota1 float, nota2 float, nota3 float, nota4 float)
    RETURNS float
    BEGIN
      
      DECLARE valrmedia float DEFAULT 0;

      SET valrmedia = (nota1 + nota2 + nota3 + nota4)/4;                          

      RETURN valrmedia;
    END //
  DELIMITER ;

  SELECT media(6,12,4,5);

/**
15.- Función mínimo
Realizar una función que reciba como argumentos 4 notas.
Debe devolver la nota mínima.
**/
DELIMITER //
  CREATE OR REPLACE FUNCTION minimo(nota1 int, nota2 int, nota3 int, nota4 int)
    RETURNS int
    BEGIN
      
      DECLARE rmin int;

      SET rmin =LEAST(n1, n2, n3, n4);                       

      RETURN rmin;
    END //
  DELIMITER ;

SELECT minimo(6,12,4,5);

/**
16.- Función máximo
Realizar una función que reciba como argumentos 4 notas.
Debe devolver la nota máxima.
**/

DELIMITER //
  CREATE OR REPLACE FUNCTION maximo(nota1 float, nota2 float, nota3 float, nota4 float)
    RETURNS float
    BEGIN
      DECLARE rmax float DEFAULT 0;

     SET rmax = GREATEST(nota1,nota2,nota3,nota4);
  
      RETURN rmax;
    END //
  DELIMITER ;

  SELECT maximo(6,12,4,5);

